# Node.js Microservice Template

This repository is useful for building microservice applications.

```plantuml
skinparam backgroundColor White
skinparam sequenceMessageAlign center

skinparam sequence {
	ArrowColor MidnightBlue
	ActorBorderColor MidnightBlue
	LifeLineBorderColor Crimson
	LifeLineBackgroundColor MidnightBlue
	
	ParticipantBorderColor Crimson
	ParticipantBackgroundColor MidnightBlue
	ParticipantFontColor White
	ParticipantFontStyle Bold
	
	ActorBackgroundColor MidnightBlue
	ActorFontColor MidnightBlue
	ActorFontSize 17
	ActorFontName Aapex
}

dummy_service_1 -> dummy_service_2 : Send a message
dummy_service_1 <-- dummy_service_2 : Receive an answer
``` 
