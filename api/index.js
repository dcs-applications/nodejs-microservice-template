const express = require('express');
const healthRouter = require('./health');
const echoRouter = require('./echo');

const router = express.Router();

router.use('/health', healthRouter);
router.use('/echo', echoRouter);

module.exports = router;
